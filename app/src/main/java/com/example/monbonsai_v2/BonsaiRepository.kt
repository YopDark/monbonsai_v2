package com.example.monbonsai_v2

import android.net.Uri
import com.example.monbonsai_v2.BonsaiRepository.Singleton.bonsaiList
import com.example.monbonsai_v2.BonsaiRepository.Singleton.databaseRef
import com.example.monbonsai_v2.model.BonsaiModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class BonsaiRepository {
    object Singleton {

        //Se connecter a la reference plants
        val databaseRef = FirebaseDatabase.getInstance()
            .getReferenceFromUrl("https://monbonsai-v2-default-rtdb.europe-west1.firebasedatabase.app/bonsai")

        //créer une liste qui va contenir nos plantes
        val bonsaiList = arrayListOf<BonsaiModel>()

    }

    fun updateData(callback: ()-> Unit){
        //absorber les données depuis la databaseRef -> liste bonsai
        databaseRef.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                //retirer les anciennes
                bonsaiList.clear()

                //récolter la liste
                for (ds in snapshot.children){
                    //construire un objet bonsai
                    val bonsai = ds.getValue(BonsaiModel::class.java)

                    //vérifier que le bonsai n'est pas null
                    if(bonsai != null){
                        bonsaiList.add(bonsai)
                    }
                }
                //actionner le callback
                callback()
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })
    }

    //mettre a jour un objet
    fun updateBonsai(bonsai: BonsaiModel){
        databaseRef.child(bonsai.id).setValue(bonsai)
    }
}