package com.example.monbonsai_v2.model

class BonsaiModel (
    val id: String = "bonsai0",
    val arrosage: String = "Le ficus doit être arrosé normalement, c’est-à-dire qu’il doit être arrosé généreusement dès que le substrat s’assèche légèrement. Le Bonsaï de ficus peut tolérer occasionnellement le sur- ou sous-arrosage. Une eau douce à température ambiante est parfaite. Il est conseillé de brumiser quotidiennement pour maintenir l’humidité, sans exagération sous peine de problèmes fongiques. Plus chaud est l’emplacement du figuier en hiver, plus il est nécessaire de donner de l’eau. Si l’arbre passe l’hiver dans un endroit plus frais, il ne demandera qu’à rester légèrement humide.",
    val description: String = "L'érable japonais (Acer palmatum) est originair...",
    val emplacement: String = "L'érable japonais préfère une position ensoleil...",
    val engrais: String = "Surtout pour les bonsaï d'érable japonais matur...",
    val feuillage: String = "Caduc",
    val maladie: String = "l'érable japonais est une espèce d'arbre assez ...",
    val multiplication: String ="L'érable japonais peut facilement se multiplier...",
    val nom_fr:String = "Erable du Japon",
    val nom_jap: String ="Acer Palamatum",
    val rempotage: String ="rempoter le une fois tous les deux ans et taill...",
    val taille: String ="La taille des pousses et des rameaux peut être ...",
    var liked: Boolean = false,
    val imageUrl: String = ""

        ){


}