package com.example.monbonsai_v2.ui.identifier

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.monbonsai_v2.MainActivity
import com.example.monbonsai_v2.R

class IdentifierFragment (
    private val context: MainActivity

): Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_identifier, container, false)

        return view

    }
}