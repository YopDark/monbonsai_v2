package com.example.monbonsai_v2.ui.collection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.monbonsai_v2.BonsaiRepository.Singleton.bonsaiList
import com.example.monbonsai_v2.MainActivity
import com.example.monbonsai_v2.R
import com.example.monbonsai_v2.adapter.CollectionAdapter
import com.example.monbonsai_v2.databinding.FragmentCollectionBinding

class CollectionFragment (
    private val context: MainActivity

): Fragment() {

   /* private var _binding: FragmentCollectionBinding? = null

    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    private var imageViewCollection : ImageView? = null


*/
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       /* _binding = FragmentCollectionBinding.inflate(inflater, container, false)
        val view = binding.root*/

       val view = inflater?.inflate(R.layout.fragment_collection, container, false)

        //récuper le recyclerview
        val verticalRecyclerView = view.findViewById<RecyclerView>(R.id.collection_recycler_list)
        verticalRecyclerView.adapter = CollectionAdapter(context, bonsaiList.filter{ it.liked }, R.layout.item_vertical_collection)
       verticalRecyclerView.layoutManager = LinearLayoutManager(context)

        return view
    }





}