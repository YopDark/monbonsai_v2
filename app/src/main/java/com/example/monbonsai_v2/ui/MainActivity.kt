package com.example.monbonsai_v2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.monbonsai_v2.databinding.ActivityMainBinding
import com.example.monbonsai_v2.ui.HomeFragment
import com.example.monbonsai_v2.ui.collection.CollectionFragment
import com.example.monbonsai_v2.ui.identifier.IdentifierFragment
import com.google.android.material.bottomappbar.BottomAppBar

class MainActivity : AppCompatActivity() {

    private lateinit var mBottomAppBar: BottomAppBar
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //charger notre repository
        val repo = BonsaiRepository()

        //mettre a jour la liste de bonsai
        repo.updateData{
            loadFragment(HomeFragment(this))
        }



        mBottomAppBar = binding.bottomAppBar
        mBottomAppBar.replaceMenu(R.menu.bottom_navigation_menu)
        mBottomAppBar.setOnMenuItemClickListener  { menuItem ->
            when (menuItem.itemId) {
                R.id.home -> {
                    loadFragment(HomeFragment(this))
                    true
                }
                R.id.collect -> {
                    loadFragment(CollectionFragment(this))
                    true
                }
                R.id.search -> {
                    loadFragment(IdentifierFragment(this))
                    true
                }
                else -> false
            }
        }

    }

    private fun loadFragment(fragment: Fragment) {

        //mettre a jour la liste de plante
        //injecter le fragment dans notre boite(fragment_container)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.content_main, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }


}

