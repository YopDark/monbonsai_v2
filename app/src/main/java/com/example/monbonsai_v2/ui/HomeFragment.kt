package com.example.monbonsai_v2.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.monbonsai_v2.BonsaiRepository.Singleton.bonsaiList
import com.example.monbonsai_v2.MainActivity
import com.example.monbonsai_v2.R
import com.example.monbonsai_v2.adapter.BonsaiAdapter
import com.example.monbonsai_v2.databinding.FragmentHomeBinding
import com.example.monbonsai_v2.model.BonsaiModel


class HomeFragment(
    private val context: MainActivity


) :Fragment() {
    private var _binding: FragmentHomeBinding? = null
    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    private var ButtonCommentFaire: CardView? = null
    private var ButtonCalendrier: CardView? = null
    private var ButtonInspiration: CardView? = null
    private var ButtonStyleBonsai: CardView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val view = binding.root
/*
        //LOCAL SANS BDD
        //Créer une liste qui va stocker ces plantes
        val bonsaiList = arrayListOf<BonsaiModel>()

        //Enregistrer une premiere plante dans la liste
        bonsaiList.add(
            BonsaiModel(
                "bonsai01",
                "n érable japonais dans un pot à bonsaï doit êtr...",
            "L'érable japonais (Acer palmatum) est originair...",
            "L'érable japonais préfère une position ensoleil...",
            "Surtout pour les bonsaï d'érable japonais matur...",
            "Sapindacée",
            "Caduc",
            "Mai à Juin",
            "Protégez l'ecorce car elle est fragile, le haub...",
            "l'érable japonais est une espèce d'arbre assez ...",
            "L'érable japonais peut facilement se multiplier...",
            "Erable du Japon",
            "Acer Palamatum",
            "rempoter le une fois tous les deux ans et taill...",
            "La taille des pousses et des rameaux peut être ...",
            false,
            "https://www.bonsaiempire.fr/great-bonsai-album/acer-bonsai-pall.jpg"

            )
        )

        //Enregistrer une premiere plante dans la liste
        bonsaiList.add(
            BonsaiModel(
                "bonsai02",
                "veillez à ne pas trop arroser, car les pins bonsaï n'aiment pas l'humidité permanente. Un bon drainage est nécessaire. Protégez les arbres contre les pluies excessives pendant que la deuxième pousse se développe, car beaucoup d'eau fera pousser les aiguilles plus longues que nécessaire",
                "L'érable japonais (Acer palmatum) est originair...",
                "L'érable japonais préfère une position ensoleil...",
                "Surtout pour les bonsaï d'érable japonais matur...",
                "Sapindacée",
                "Conifères et pins",
                "Mai à Juin",
                "Protégez l'ecorce car elle est fragile, le haub...",
                "l'érable japonais est une espèce d'arbre assez ...",
                "L'érable japonais peut facilement se multiplier...",
                "Espèce de Pin Bonsaï",
                "Pinus",
                "rempoter le une fois tous les deux ans et taill...",
                "La taille des pousses et des rameaux peut être ...",
                true,
                "https://bonsaimontreal.com/wp-content/uploads/2019/01/Pin-blanc.jpg"

            )
        )*/



        //
        //récuper le recyclerview
        val horizontalRecyclerView = view.findViewById<RecyclerView>(R.id.horizontal_recyclerview)
        horizontalRecyclerView.adapter = BonsaiAdapter(context, bonsaiList, R.layout.item_horizontal)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initViews() {
        ButtonCommentFaire = binding.cardViewCommentFaire
        ButtonCalendrier = binding.cardViewCalendrier
        ButtonInspiration = binding.cardViewInspiration
        ButtonStyleBonsai = binding.cardViewStyleBonsai
    }

    private fun initListeners() {
        ButtonCommentFaire?.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_commentFaireFragment)
        }
        ButtonCalendrier?.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_calendrierFragment)
        }
        ButtonInspiration?.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_inspirationFragment)
        }
        ButtonStyleBonsai?.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_styleBonsaiFragment)
        }

    }
}