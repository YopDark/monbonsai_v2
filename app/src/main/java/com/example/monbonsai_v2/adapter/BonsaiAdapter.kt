package com.example.monbonsai_v2.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.monbonsai_v2.BonsaiRepository
import com.example.monbonsai_v2.MainActivity
import com.example.monbonsai_v2.R
import com.example.monbonsai_v2.model.BonsaiModel


class BonsaiAdapter(
    private val context: MainActivity,
    private val bonsaiList: List<BonsaiModel>,
    private val layoutId: Int

) : RecyclerView.Adapter<BonsaiAdapter.ViewHolder>(){

    //Boite pour ranger tous les composant à controler
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        //image de la plante?
        val bonsaiImage = view.findViewById<ImageView>(R.id.image_item)
        val starIcon = view.findViewById<ImageView>(R.id.star_icon)


    }

    //Injecter notre layout
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_horizontal, parent, false)

        return ViewHolder(view)
    }

    //Mettre a jour chaque model avec notre plante
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //Récupérer les informations du bonsai
        val currentBonsai = bonsaiList[position]

        //récuperer le repository
        val repo = BonsaiRepository()

        Glide.with(context).load(Uri.parse(currentBonsai.imageUrl)).into(holder.bonsaiImage)

        //liké ou non
        if (currentBonsai.liked){
            holder.starIcon.setImageResource(R.drawable.ic_fav)
        }
        else{
            holder.starIcon.setImageResource(R.drawable.ic_unfav)

        }

        //rejouter une interaction avec le like
        holder.starIcon.setOnClickListener{
            //inverser si le bouton est like ou non
            currentBonsai.liked = !currentBonsai.liked
            //mettre a jour l'objet plant
            repo.updateBonsai(currentBonsai)
        }
    }

    //Renvoyer combien d'item dynamiquement
    override fun getItemCount(): Int = bonsaiList.size
}