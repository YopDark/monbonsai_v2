package com.example.monbonsai_v2.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.monbonsai_v2.BonsaiRepository
import com.example.monbonsai_v2.MainActivity
import com.example.monbonsai_v2.R
import com.example.monbonsai_v2.databinding.FragmentHomeBinding
import com.example.monbonsai_v2.databinding.ItemVerticalCollectionBinding
import com.example.monbonsai_v2.model.BonsaiModel

class CollectionAdapter(
    private val context: MainActivity,
    private val bonsaiList: List<BonsaiModel>,
    private val layoutId: Int

) : RecyclerView.Adapter<CollectionAdapter.ViewHolder>(){

    //Boite pour ranger tous les composant à controler
    class ViewHolder(val binding: ItemVerticalCollectionBinding) : RecyclerView.ViewHolder(binding.root){

        //image de la plante?
        val bonsaiImage = binding.imageViewCollection
        val bonsaiName = binding.nomBonsai
        val bonsaiNameJap = binding.nomJapBonsai

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemVerticalCollectionBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //Récupérer les informations du bonsai
        val currentBonsai = bonsaiList[position]

        //récuperer le repository
        val repo = BonsaiRepository()

        Glide.with(context).load(Uri.parse(currentBonsai.imageUrl)).into(holder.bonsaiImage)

        //Mettre à jour le nom de la plante
        holder.bonsaiName?.text = currentBonsai.nom_fr
        //Mettre à jour la description de la plante
        holder.bonsaiNameJap?.text = currentBonsai.nom_jap

    }

    override fun getItemCount(): Int = bonsaiList.size
}